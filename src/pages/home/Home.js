import React, { Component } from 'react';
import Form from 'react-bootstrap/Form';
import Image from 'react-bootstrap/Image';
import Pagination from 'react-bootstrap/Pagination';
import { Link } from 'react-router-dom';
import { FormApi } from '../../modules/make-api-call/form-api';
import setVariables from '../../modules/utils/set-variables';
import { withParams } from '../../components/with-params';
import sortBy from '../../modules/utils/sort-by';

class Home extends Component {
    constructor(props) {
        super(props);

        this.handleSortingChange = this.handleSortingChange.bind(this);
        this.state = {
            pokemons: [],
            sortOptions: [
                {
                    id: 1,
                    value: 'asc',
                    text: 'Ascending',
                },
                {
                    id: 2,
                    value: 'desc',
                    text: 'Descending',
                },
            ],
            sort: 'asc',
            pagination: {
                lower: 1,
                upper: 10,
                page: 1,
            }
        };

    }

    componentDidMount() {

        let page = parseInt(this.props.params.page),
            pagination,
            diff,
            high,
            low,
            lower,
            upper;

        page = (typeof page === 'number') ? ((page * 10 <= 905) ? page : 91) : 1;
        diff = Math.floor(page/10);
        lower = (diff * 10) - 1;
        lower = (lower <= 0) ? 1 : lower;
        upper = ((diff+1) * 10) + 1;
        upper = (upper >= 905) ? 905 : upper;

        if (page === 1) {
            low = 1;
            high = 10;
        } else {
            low = (page * 10) + 1;
            high = ((page+1) * 10) + 1;
        }

        let formApiList = [];
        for (let i = low; i < high; i++) {
            formApiList[i-1] = FormApi({ url: "/" + i});
        }

        Promise.all(formApiList).then((responses) => {
            let pokemons = [];

            responses.forEach((response, index) => {

                if (!(response === null || typeof response === 'undefined')) {
                    pokemons.push({
                        id: setVariables(response.data.id, 0),
                        name: setVariables(response.data.name, ""),
                        pokemon_url: setVariables(response.data.pokemon.url, ""),
                        photo_url: setVariables(response.data.sprites.front_default, ""),
                    });
                }

            });

            this.setState({ pokemons : pokemons });

        }).catch((error) => {
            this.setState({ error : error });
        });

        pagination = { lower: lower, upper: upper, page: page };
        this.setState({ pagination : pagination });

    }

    handleSortingChange(event) {
        const { value } = event.target;
        let pokemons = [];

        if (value === 'asc') {
            pokemons = this.state.pokemons.sort(sortBy('name', false, (pokemon) =>  pokemon.toUpperCase()))
            this.setState({ pokemons : pokemons });
        }

        if (value === 'desc') {
            pokemons = this.state.pokemons.sort(sortBy('name', true, (pokemon) =>  pokemon.toUpperCase()))
            this.setState({ pokemons : pokemons });
        }

    }

    render() {

        const { pokemons, sortOptions, pagination } = this.state;

        let active = pagination.page,
            counter = 0,
            end = pagination.upper - pagination.lower,
            items = [];

        for (let number = pagination.lower; number <= pagination.upper; number++) {
            let label = number;
            if (number > 1 && counter === 0) {
                label = "Previous";
            }

            if (number < 905 && counter === end) {
                label = "Next";
            }

            items.push(
                <Pagination.Item href={`/${number}`} key={number} active={number === active}>
                    {label}
                </Pagination.Item>,
            );
            counter++;
        }

        return (
            <section className="Home" style={{ backgroundColor: '#f4f5f7' }}>
                <div className="container py-5 h-100">
                    <div className="row d-flex justify-content-center align-items-center h-100">
                        <div className="col col-lg-6 mb-4 mb-lg-0">
                            <div className="card mb-3" style={{ borderRadius: '.5rem' }}>
                                <div className="row g-0">
                                    <div className="col-12">
                                        <Form className="card-body p-4">
                                            <div className="mb-3 row">
                                                <label htmlFor="sort"
                                                       className="col-8 col-form-label text-end">Sort</label>
                                                <div className="col-4">
                                                    <Form.Select onChange={this.handleSortingChange}>
                                                        {Array.isArray(sortOptions) && sortOptions.map(sortOption =>
                                                            <option key={sortOption.id} value={sortOption.value}>{sortOption.text}</option>
                                                        )}
                                                    </Form.Select>
                                                </div>
                                            </div>
                                            <div className="col-12">
                                                <div className="list-group list-group-flush">
                                                    {Array.isArray(pokemons) && pokemons.map(pokemon =>
                                                        <Link key={pokemon.id} to={`/pokemon/${pokemon.name}`} className="list-group-item">
                                                            <Image src={pokemon.photo_url} height="50" width="50" />
                                                            <span>{pokemon.name}</span>
                                                        </Link>
                                                    )}
                                                </div>
                                            </div>
                                        </Form>
                                    </div>
                                </div>
                            </div>
                            <div className="col-12  d-flex justify-content-center align-items-center">
                                <Pagination>
                                    {items}
                                </Pagination>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default withParams(Home);
