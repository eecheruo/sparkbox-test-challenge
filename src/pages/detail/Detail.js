import React, { Component } from 'react';
import Image from "react-bootstrap/Image";
import Stat from '../../components/Stat';
import Ability from '../../components/Ability';
import Favorite from '../../components/Favorite';
import { Link } from 'react-router-dom';
import { PokemonApi } from '../../modules/make-api-call/pokemon-api';
import setVariables from '../../modules/utils/set-variables';
import { withParams } from '../../components/with-params';

class Detail extends Component {
    constructor(props) {
        super(props);

        this.state = {
            pokemon: {
                id: null,
                slug: null,
                photo_url: null,
                name: null,
                weight: 0,
                height: 0,
                abilities: [],
                stats: []
            }
        };

    }

    componentDidMount() {

        const slug = this.props.params.slug;

        PokemonApi({ url : "/" + slug }).then((response) => {

            let photo_url = setVariables(response['data']['sprites']['other']['official-artwork']['front_default'], ''),
                id = setVariables(response.data.id, 0),
                name = setVariables(response.data.name, ''),
                weight = setVariables(response.data.weight, 0),
                height = setVariables(response.data.height, 0);

            let abilities = setVariables(response.data.abilities, []);
            if (abilities.length > 0) {
                abilities = abilities.map((ability) => {
                    return {
                        name: ability.ability.name,
                        is_hidden: ability.is_hidden
                    };
                });
            }

            let stats = setVariables(response.data.stats, []);
            if (stats.length > 0) {
                stats = stats.map((stat) => {
                    return {
                        base_stat: stat.base_stat,
                        name: stat.stat.name
                    };
                })
            }

            let pokemon = {
                id: id,
                slug: slug,
                photo_url: photo_url,
                name: name,
                weight: weight,
                height: height,
                abilities: abilities,
                stats: stats
            };

            this.setState({ pokemon: pokemon })

        }).then((error) => {

            console.log(error);

        })

    }

    render() {

        const { pokemon } = this.state;

        return (
            <section className="Detail vh-100" style={{ backgroundColor: '#f4f5f7' }}>
                <div className="container py-5 h-100">
                    <div className="row d-flex justify-content-center align-items-center h-100">
                        <div className="col col-lg-6 mb-4 mb-lg-0">
                            <div className="card mb-3" style={{ borderRadius: '.5rem' }}>
                                <div className="row g-0">
                                    <div className="col-md-4 gradient-custom text-center text-white" style={{ borderTopLeftRadius: '.5rem', borderBottomLeftRadius: '.5rem' }}>
                                        <Image src={pokemon.photo_url} alt={pokemon.name} className="img-fluid my-5" style={{ width: '80px' }} />
                                        <h5>{pokemon.name}</h5>
                                        <div className="col-12 mb-3">
                                            <Link to={`/${((Math.floor(pokemon.id/10) === 0) ? 1 : Math.floor(pokemon.id/10))}`} className="text-white text-decoration-none mb-3">see list <i className="fa fa-list"></i></Link>
                                        </div>
                                    </div>
                                    <div className="col-md-8">
                                        <div className="card-body p-4">
                                            <Favorite slug={pokemon.slug} />
                                            <div className="row pt-1">
                                                <div className="col-6 mb-3">
                                                    <h6>Height</h6>
                                                    <p className="text-muted">{pokemon.height}</p>
                                                </div>
                                                <div className="col-6 mb-3">
                                                    <h6>Weight</h6>
                                                    <p className="text-muted">{pokemon.weight}</p>
                                                </div>
                                            </div>
                                            <h6>Stats</h6>
                                            <hr className="mt-0 mb-4" />
                                            {Array.isArray(pokemon.stats) && pokemon.stats.map((stat, index) =>
                                                <Stat key={index} stat={stat} />
                                            )}
                                            <h6>Abilities</h6>
                                            <hr className="mt-0 mb-4" />
                                            <div className="row pt-1 pb-3">
                                            {Array.isArray(pokemon.abilities) && pokemon.abilities.map((ability, index) =>
                                                <Ability key={index} ability={ability} />
                                            )}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default withParams(Detail);
