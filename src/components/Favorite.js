import React, { Component } from 'react';

class Favorite extends Component {
    constructor(props) {
        super(props);
        this.handleFavoriteClick = this.handleFavoriteClick.bind(this);
        this.state = {
            slug: null,
            colorSwitch: 'text-primary fa fa-heart fa-lg me-3',
            favorite: [],
        }

    }

    componentDidMount() {
        this.initializing();
    }

    initializing() {

        let parsed = new URL(document.location.href),
            parsedArr = parsed.pathname.split('/'),
            slug = parsedArr[parsedArr.length-1],
            favorite = localStorage.getItem('favorite'),
            entry = [{ name: slug, like : false}];

        if (favorite == null || typeof favorite == 'undefined') {
            if (!(slug == null || typeof slug == 'undefined')) {
                this.setState({ favorite: entry, slug: slug });
                favorite = JSON.stringify(entry);
                localStorage.setItem('favorite', favorite);
            }
        } else {

            let colorSwitch = 'none',
                favorites = [],
                found = false;

            favorites = JSON.parse(favorite);

            favorites = favorites.filter((favorite) => {

                if (favorite.name === slug && !(slug === null || typeof slug === 'undefined')) {
                    colorSwitch = (favorite.like) ? 'true' : 'false';
                    found = true;
                }

                if (favorite.name != null) {
                    return favorite;
                }

                return false;
            });

            if (!(slug === null || typeof slug === 'undefined')) {
                this.setState({ slug: slug });
                if (!found) {
                    favorites.push({ name: slug, like : false});
                    localStorage.setItem('favorite', JSON.stringify(favorites));
                }
            }

            if (colorSwitch === 'false') {
                this.setState({
                    colorSwitch: 'text-primary fa fa-heart fa-lg me-3',
                    favorite: favorites,
                });
            }

            if (colorSwitch === 'true') {
                this.setState({
                    colorSwitch: 'text-danger fa fa-heart fa-lg me-3',
                    favorite: favorites,
                });
            }

        }

    }

    handleFavoriteClick() {
        let colorSwitch = 'none',
            favorites = [];

        this.initializing();

        const { slug, favorite } = this.state;
        if (!(slug === null || typeof slug === 'undefined')) {
            favorites = favorite.map((fav) => {
                if (fav.name === this.props.slug) {
                    if (fav.like) {
                        colorSwitch = 'true';
                        fav.like = false;
                        return fav;
                    } else {
                        colorSwitch = 'false';
                        fav.like = true;
                        return fav;
                    }
                } else {
                    return fav;
                }
            });

            if (colorSwitch === 'true') {
                this.setState({
                    colorSwitch: 'text-primary fa fa-heart fa-lg me-3',
                    favorite: favorites,
                });
                localStorage.setItem('favorite', JSON.stringify(favorites));
            }

            if (colorSwitch === 'false') {
                this.setState({
                    colorSwitch: 'text-danger fa fa-heart fa-lg me-3',
                    favorite: favorites,
                });
                localStorage.setItem('favorite', JSON.stringify(favorites));
            }

        }

    }

    render() {
        return (
            <>
                <div className="d-flex justify-content-end">
                    <i onClick={this.handleFavoriteClick} className={ this.state.colorSwitch } style={{ cursor: 'pointer' }}></i>
                </div>
            </>
        );
    }
}

export default Favorite;