import React, { Component } from 'react';
import ProgressBar from 'react-bootstrap/ProgressBar';

class Stat extends Component {
    render() {
        const { stat } = this.props;
        return (
            <>
                <h6 className="col-4 mb-3">{stat.name}</h6>
                <ProgressBar className="col-8 mb-3" now={stat.base_stat} label={`${stat.base_stat}`} />
            </>
        );
    }
}

export default Stat;