import React, { Component } from 'react';

class Ability extends Component {
    render() {
        const { ability } = this.props;
        return (
            <>
                <h6 className="text-muted">{ability.name} {ability.is_hidden && <span className="badge bg-secondary">hidden</span> }</h6>
            </>
        );
    }
}

export default Ability;