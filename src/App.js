import React, { Component } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Home from './pages/home/Home';
import Detail from './pages/detail/Detail';
import './App.css';

class App extends Component {

    render() {
        return (
            <React.Fragment>
              <BrowserRouter>
                <Routes>
                    <Route path="/:page" element={<Home />} />
                    <Route path="/pokemon/:slug" element={<Detail />} />
                </Routes>
              </BrowserRouter>
            </React.Fragment>
        );
    }

}

export default App;
