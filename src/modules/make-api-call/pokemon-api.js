import defaultConfig from '../../configs/config';
import makeApiCall from './make-api-call';

export const PokemonApi = (config) => {
    config.method = "get"
    config.baseURL = defaultConfig.baseUrl + "/pokemon";
    return makeApiCall(config);
}
