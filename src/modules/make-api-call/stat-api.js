import defaultConfig from '../../configs/config';
import makeApiCall from './make-api-call';

export const StatApi = (config) => {
    config.method = "get"
    config.baseURL = defaultConfig.baseUrl + "/stat";
    return makeApiCall(config);
}
