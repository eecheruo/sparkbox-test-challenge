import defaultConfig from '../../configs/config';
import makeApiCall from './make-api-call';

export const FormApi = (config) => {
    config.method = "get"
    config.baseURL = defaultConfig.baseUrl + "/pokemon-form";
    return makeApiCall(config);
}
