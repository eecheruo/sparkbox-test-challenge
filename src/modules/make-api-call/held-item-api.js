import defaultConfig from '../../configs/config';
import makeApiCall from './make-api-call';

export const HeldItemApi = (config) => {
    config.method = "get"
    config.baseURL = defaultConfig.baseUrl + "/item";
    return makeApiCall(config);
}
