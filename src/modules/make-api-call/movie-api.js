import defaultConfig from '../../configs/config';
import makeApiCall from './make-api-call';

export const MoveApi = (config) => {
    config.method = "get"
    config.baseURL = defaultConfig.baseUrl + "/move";
    return makeApiCall(config);
}
