import defaultConfig from '../../configs/config';
import makeApiCall from './make-api-call';

export const VersionApi = (config) => {
    config.method = "get"
    config.baseURL = defaultConfig.baseUrl + "/version";
    return makeApiCall(config);
}
