import defaultConfig from '../../configs/config';
import axios from 'axios';

const makeApiCall = (config) => {

    axios.interceptors.response.use((response) => {
        return response;
    }, (error) => {
        return Promise.reject(error);
    })

    if (typeof config.baseURL == 'undefined') {
        config.baseURL = defaultConfig.baseUrl;
    }

    return axios(config);
};

export default makeApiCall;
