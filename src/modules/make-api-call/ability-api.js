import defaultConfig from '../../configs/config';
import makeApiCall from './make-api-call';

export const AbilityApi = (config) => {
    config.method = "get"
    config.baseURL = defaultConfig.baseUrl + "/ability";
    return makeApiCall(config);
}
