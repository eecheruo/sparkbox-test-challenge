
const setVariables = (variable, defaults) => {
    return (typeof variable !== 'undefined') ? variable : defaults;
};

export default setVariables;
