
const sortBy = (field, direction, typeCast) => {
    const key = typeCast ? function(x) {
        return typeCast(x[field])
    } : function(x) {
        return x[field]
    };

    direction = !direction ? 1 : -1;
    return function(a, b) {
        return a = key(a), b = key(b), direction * ((a > b) - (b > a));
    }
}

export default sortBy;
