import React from 'react';
import { expect, test } from '@jest/globals';
import sortBy from '../../modules/utils/sort-by';

const pokemons = [
    {
        "id": 201,
        "name": "unown-a",
        "pokemon_url": "https://pokeapi.co/api/v2/pokemon/201/",
        "photo_url": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/201.png"
    },
    {
        "id": 202,
        "name": "wobbuffet",
        "pokemon_url": "https://pokeapi.co/api/v2/pokemon/202/",
        "photo_url": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/202.png"
    },
    {
        "id": 203,
        "name": "girafarig",
        "pokemon_url": "https://pokeapi.co/api/v2/pokemon/203/",
        "photo_url": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/203.png"
    },
    {
        "id": 204,
        "name": "pineco",
        "pokemon_url": "https://pokeapi.co/api/v2/pokemon/204/",
        "photo_url": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/204.png"
    },
    {
        "id": 205,
        "name": "forretress",
        "pokemon_url": "https://pokeapi.co/api/v2/pokemon/205/",
        "photo_url": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/205.png"
    },
    {
        "id": 206,
        "name": "dunsparce",
        "pokemon_url": "https://pokeapi.co/api/v2/pokemon/206/",
        "photo_url": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/206.png"
    },
    {
        "id": 207,
        "name": "gligar",
        "pokemon_url": "https://pokeapi.co/api/v2/pokemon/207/",
        "photo_url": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/207.png"
    },
    {
        "id": 208,
        "name": "steelix",
        "pokemon_url": "https://pokeapi.co/api/v2/pokemon/208/",
        "photo_url": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/208.png"
    },
    {
        "id": 209,
        "name": "snubbull",
        "pokemon_url": "https://pokeapi.co/api/v2/pokemon/209/",
        "photo_url": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/209.png"
    },
    {
        "id": 210,
        "name": "granbull",
        "pokemon_url": "https://pokeapi.co/api/v2/pokemon/210/",
        "photo_url": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/210.png"
    }
];

describe('sort unit test', () => {
    test('sort by name ascending', () => {

        expect([
            {
                "id": 206,
                "name": "dunsparce",
                "pokemon_url": "https://pokeapi.co/api/v2/pokemon/206/",
                "photo_url": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/206.png"
            },
            {
                "id": 205,
                "name": "forretress",
                "pokemon_url": "https://pokeapi.co/api/v2/pokemon/205/",
                "photo_url": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/205.png"
            },
            {
                "id": 203,
                "name": "girafarig",
                "pokemon_url": "https://pokeapi.co/api/v2/pokemon/203/",
                "photo_url": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/203.png"
            },
            {
                "id": 207,
                "name": "gligar",
                "pokemon_url": "https://pokeapi.co/api/v2/pokemon/207/",
                "photo_url": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/207.png"
            },
            {
                "id": 210,
                "name": "granbull",
                "pokemon_url": "https://pokeapi.co/api/v2/pokemon/210/",
                "photo_url": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/210.png"
            },
            {
                "id": 204,
                "name": "pineco",
                "pokemon_url": "https://pokeapi.co/api/v2/pokemon/204/",
                "photo_url": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/204.png"
            },
            {
                "id": 209,
                "name": "snubbull",
                "pokemon_url": "https://pokeapi.co/api/v2/pokemon/209/",
                "photo_url": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/209.png"
            },
            {
                "id": 208,
                "name": "steelix",
                "pokemon_url": "https://pokeapi.co/api/v2/pokemon/208/",
                "photo_url": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/208.png"
            },
            {
                "id": 201,
                "name": "unown-a",
                "pokemon_url": "https://pokeapi.co/api/v2/pokemon/201/",
                "photo_url": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/201.png"
            },
            {
                "id": 202,
                "name": "wobbuffet",
                "pokemon_url": "https://pokeapi.co/api/v2/pokemon/202/",
                "photo_url": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/202.png"
            }
        ]).toStrictEqual(pokemons.sort(sortBy('name', false, (pokemon) =>  pokemon.toUpperCase())));

    });

    test('sort by name descending', () => {

        expect([
            {
                "id": 202,
                "name": "wobbuffet",
                "pokemon_url": "https://pokeapi.co/api/v2/pokemon/202/",
                "photo_url": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/202.png"
            },
            {
                "id": 201,
                "name": "unown-a",
                "pokemon_url": "https://pokeapi.co/api/v2/pokemon/201/",
                "photo_url": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/201.png"
            },
            {
                "id": 208,
                "name": "steelix",
                "pokemon_url": "https://pokeapi.co/api/v2/pokemon/208/",
                "photo_url": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/208.png"
            },
            {
                "id": 209,
                "name": "snubbull",
                "pokemon_url": "https://pokeapi.co/api/v2/pokemon/209/",
                "photo_url": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/209.png"
            },
            {
                "id": 204,
                "name": "pineco",
                "pokemon_url": "https://pokeapi.co/api/v2/pokemon/204/",
                "photo_url": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/204.png"
            },
            {
                "id": 210,
                "name": "granbull",
                "pokemon_url": "https://pokeapi.co/api/v2/pokemon/210/",
                "photo_url": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/210.png"
            },
            {
                "id": 207,
                "name": "gligar",
                "pokemon_url": "https://pokeapi.co/api/v2/pokemon/207/",
                "photo_url": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/207.png"
            },
            {
                "id": 203,
                "name": "girafarig",
                "pokemon_url": "https://pokeapi.co/api/v2/pokemon/203/",
                "photo_url": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/203.png"
            },
            {
                "id": 205,
                "name": "forretress",
                "pokemon_url": "https://pokeapi.co/api/v2/pokemon/205/",
                "photo_url": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/205.png"
            },
            {
                "id": 206,
                "name": "dunsparce",
                "pokemon_url": "https://pokeapi.co/api/v2/pokemon/206/",
                "photo_url": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/206.png"
            }
        ]).toStrictEqual(pokemons.sort(sortBy('name', true, (pokemon) =>  pokemon.toUpperCase())));

    });

});

